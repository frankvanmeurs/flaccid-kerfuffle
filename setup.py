import setuptools

setuptools.setup(
    name="yogi-bearer-proxy",
    install_requires=["flask"],
    python_requires=">=3.6",
    packages=setuptools.find_packages(
        where="src",
        include=["yogibearerproxy"],
        exclude=["yogibearerproxytests"]
    ),
    package_dir={"": "src"},
    entry_points={
        'console_scripts': [
            "yogi-bearer-proxy = yogibearerproxy:run"
        ]
    }
)
