from logging.config import dictConfig

from flask import Flask, current_app
from werkzeug.local import LocalProxy

logger = LocalProxy(lambda: current_app.logger)

dictConfig({
    "version": 1,
    "ROOT": {
        "level": "info"
    }
})

app: Flask


def create_app() -> Flask:
    _app = Flask(__name__, instance_relative_config=True)

    _app.config.from_mapping(
        ENV="dev",
    )

    from . import proxy

    _app.register_blueprint(proxy.bp)

    return _app


app = create_app()
