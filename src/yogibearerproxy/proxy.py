import http
import logging
from typing import Type

from flask import Response, Blueprint
from flask.views import MethodView
from werkzeug.exceptions import MethodNotAllowed

from . import logger as applogger

logger: logging.Logger
bp: Blueprint


# Define the methods on the endpoints as a MethodView
class ProxyView(MethodView):

    def get(self, ep):
        return Response("{path}".format(path=ep), http.HTTPStatus.OK)

    def post(self, ep):
        raise MethodNotAllowed()


# Add the MethodView to the Blueprint
def add_method_view(blueprint: Blueprint, methodview_class: Type[MethodView]) -> Blueprint:
    view = methodview_class.as_view("view")
    blueprint.add_url_rule("/", view_func=view, methods=["GET", "POST", ], defaults={"ep": None})
    blueprint.add_url_rule("/<path:ep>", view_func=view, methods=["GET", "POST", ])
    return blueprint


logger = applogger
bp = Blueprint("proxy", __name__)
bp = add_method_view(bp, ProxyView)
